package com.app.entities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import com.app.util.*;

public class Main implements Constants {

	public static int choice, CAR_ID, user_ID;
	static BufferedReader br;
	static HashMap<Integer, Customer> hash = new HashMap<Integer, Customer>();
	static char h;
	static String Model;
	static Car c;
	static Set<Integer> setOfKeys = hash.keySet();
	static Iterator<Integer> iterator = setOfKeys.iterator();

	public static void main(String[] args) throws Exception {

		InputStreamReader isr = new InputStreamReader(System.in);
		br = new BufferedReader(isr);

		do {

			System.out.println("*****WELCOME*****");
			System.out.println("1. Press 1 for new customers");
			System.out.println("2. Press 2 for Adding cars to existing customers");
			System.out.println("3. Press 3 for list of all customers");
			System.out.println("4. Press 4 for individual customer information");
			System.out.println("5. Press 5 for *Prizes*");
			System.out.println("6. Press 6 for exit");
			choice = Integer.parseInt(getString());

			switch (choice) {
			case ADD_USER:

				addUser();

				break;

			case ADD_CAR:
				addCar();
				break;

			case DISPLAY_ALL:
				displayAll();

				break;

			case DISPLAY:
				displayUser();
				break;
			
			  case GET_PRIZE:
			          getPrize();
			  break;
			 

			case EXIT:
				System.exit(0);
				break;
			default:

				System.out.println("Please enter a valid value");

				break;

			}

		} while (choice != EXIT);
	}

	private static void getPrize()throws Exception
	{
		// TODO Auto-generated method stub
		
		List<Integer> templist=new ArrayList<Integer>();
		System.out.println("DEAR user please add your ID for lucky draw");
		for(int i=1;i<=2;i++)
		{
	    System.out.println("Enter ID's");
		user_ID=Integer.parseInt(getString());
		templist.add(user_ID);
		}
		
		List<Integer> valuesList = new ArrayList<Integer>(hash.keySet());
		for(int j=0;j<3;j++)
		{
		int randomIndex = new Random().nextInt(valuesList.size());
		Integer randomValue = valuesList.get(randomIndex);
		System.out.println("Random values are"+randomValue);
		}
		templist.retainAll(valuesList);
		System.out.println("people who won"+templist);
		
		
		
	}

	private static void displayUser() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Enter user ID You want to see");
		int id = Integer.parseInt(getString());
		if (hash.get(id) != null) {
			Customer cust = hash.get(id);
			System.out.println("Name of Customer is\t" + cust.name);
			 for(Car C1 : cust.myList) {
//		            System.out.println("\nModel of car\t"+C1.getModel().toString());
//		            System.out.println("\nID of car\t"+C1.getCAR_ID());
//		            System.out.println("\n Price of the CAR"+C1.getPrice());
//		            System.out.println("\n Price of the CAR"+C1.getResale_val());
		            System.out.println(C1);
		        }
		
			
		}

	}

	public static void displayAll() {
		// TODO Auto-generated method stub
		for (Entry<Integer, Customer> entry : hash.entrySet()) {
			Customer c2 = entry.getValue();
			System.out.println("\n\nUserID = " + entry.getKey() + ", Name = " + c2.name);
			for(Car C1 : c2.myList) {
				System.out.println(C1);
	        }
		}

	}

	private static void addCar() throws Exception {
		// TODO Auto-generated method stub

		System.out.println("Enter User_ID");
		int id = Integer.parseInt(getString());
		// Customer cust=new Customer();

		Customer cust = hash.get(id);
		
		if (hash.get(id) != null) {

			
				System.out.println("Enter car to be added");
				System.out.println("press M for Maruti");
				System.out.println("press H for Hyundai");
				System.out.println("press T for toyota");
				System.out.println("press 6 for Exit");
				h = (char) br.read();
				switch (h) {

				case Maruti:
					System.out.println("Enter CAR_ID");
					CAR_ID=Integer.parseInt(getString());
					
					System.out.println("Enter CAR model");
					String Model = getString();
					System.out.println("Enter price");
					int price = Integer.parseInt(getString());
					
					Car c = new Maruti(CAR_ID, Model,price);
					cust.myList.add(c);
					break;
					
				case Hyundai:
					System.out.println("Enter CAR_ID");
					CAR_ID = Integer.parseInt(getString());
					System.out.println("Enter CAR model");
					Model = getString();
					System.out.println("Enter price");
					 price = Integer.parseInt(getString());
					c = new Hyundai(CAR_ID, Model,price);
					cust.myList.add(c);

					break;
				case Toyoto:
					System.out.println("Enter CAR_ID");
				
					CAR_ID = Integer.parseInt(getString());
					System.out.println("Enter CAR model");
					Model = getString();
					System.out.println("Enter price");
					 price = Integer.parseInt(getString());
					c = new Toyoto(CAR_ID, Model,price);
					cust.myList.add(c);

					break;
				/*case EXIT1:
					System.exit(0);
					break;*/
				}
			

		} else {
			System.out.println("User not found!");
		}

	}

	public static void addUser() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Enter User Name");
		String name = getString();
		System.out.println("Enter UserID");
		user_ID = Integer.parseInt(getString());
		Customer c =new Customer(user_ID, name);
		if (hash.get(user_ID) == null) {
			hash.put(user_ID, c);
		} else {
			System.out.println("ID already exists..please enter a new ID");
		}

	}
	
	

	private static String getString() throws Exception{
		String str;
		while (true) {
			 str = br.readLine();
			if (!str.isEmpty()) {
				break;
			}
		}
		return str;
	}
	
}
