package com.app.entities;

//import java.util.*;



abstract public class Car {
	 
	    
    	int CAR_ID,price,resale_val;
    	String Model;
	     
	    
	    
        public Car(int cAR_ID, String model,int Price)
        {
			
		CAR_ID = cAR_ID;
		Model = model;
		price=Price;
	  //  this.resale_val=resale_value;
		
			
		}

        
        @Override
    	public String toString() {
    		return "\nModel of car\t" + this.Model +"\n Id of the car\t"+this.CAR_ID+"\n Price of the car\t"+this.price+"\nresale value of car\t"+this.resale_val;
    	}

		public int getCAR_ID() {
			return CAR_ID;
		}


		public void setCAR_ID(int cAR_ID) {
			CAR_ID = cAR_ID;
		}


		public String getModel() {
			return Model;
		}


		public void setModel(String model) {
			Model = model;
		}


		public int getPrice() {
			return price;
		}


		public void setPrice(int price) {
			this.price = price;
		}


		public int getResale_val() {
			return resale_val;
		}


		public void setResale_val(int resale_val) {
			this.resale_val = resale_val;
		}
	    
		
	}



