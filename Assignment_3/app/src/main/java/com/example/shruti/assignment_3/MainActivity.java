package com.example.shruti.assignment_3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;



public class MainActivity extends ActionBarActivity {

    ListView listView;
    TestAdapter adapter;
    List<Student> stuList=new ArrayList<Student>();
    TextView tv1;
    String name;
    int roll;
    Context context=this;
   // Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=(ListView)findViewById(R.id.list_view);
        adapter = new TestAdapter(stuList,this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                LayoutInflater factory = LayoutInflater.from(context);
                final View DialogView = factory.inflate(
                        R.layout.dialog, null);
                final AlertDialog dialog1 = new AlertDialog.Builder(MainActivity.this).create();
                dialog1.setView(DialogView);
                DialogView.findViewById(R.id.view_btn).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                     dialog1.dismiss();



                    }
                });
                DialogView.findViewById(R.id.edit_btn).setOnClickListener(new View.OnClickListener() {

                    @Override
                       public void onClick(View view){
                        if (view.getId()>0) {

                          String valueString=  adapter.stuList.get(position).name;
                            int valueInt=adapter.stuList.get(position).roll_no;
                            Bundle sendBundle = new Bundle();
                            sendBundle.putString("name", valueString);
                            sendBundle.putInt("roll", valueInt);


                            Intent intent;
                            intent = new Intent(MainActivity.this, MainActivity2.class);
                            intent.putExtras(sendBundle);

                            startActivityForResult(intent, 1);
                            adapter.stuList.remove(position);
                            dialog1.dismiss();
                        }
                    }
                });
                DialogView.findViewById(R.id.delete_btn).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                       adapter.stuList.remove(position);
                       adapter.notifyDataSetChanged();
                       dialog1.dismiss();
                    }


                });

                dialog1.show();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 100){
            if(resultCode == RESULT_OK){

                    name = data.getStringExtra("name");
                    roll = data.getIntExtra("roll", 0);
                    adapter.stuList.add(new Student(roll, name));
                    adapter.notifyDataSetChanged();
                }





            } else if(resultCode == RESULT_CANCELED){

            }
        if(requestCode==1){
            if(resultCode==RESULT_OK){

                Bundle extras = data.getExtras();

                name = extras.getString("name");
                roll = extras.getInt("roll", 0);
                adapter.stuList.add(new Student(roll, name));
                adapter.notifyDataSetChanged();



            }


        }


    }

    public void click(View view) {

            switch(view.getId()){
                case R.id.Add_button:
                    Intent intent = new Intent(this,MainActivity2.class);

                    startActivityForResult(intent, 100);


                    break;

                default:
                    break;


            }



    }


}
