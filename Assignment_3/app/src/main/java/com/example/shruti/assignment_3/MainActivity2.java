package com.example.shruti.assignment_3;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity2 extends Activity {


   EditText Edit1;
    EditText Edit2;

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
         Edit1= (EditText) findViewById(R.id.ed1);
         Edit2= (EditText) findViewById(R.id.ed2);
         intent=this.getIntent();
        Bundle receiveBundle = this.getIntent().getExtras();
        if(receiveBundle!=null) {
            final String receiveValue1 = receiveBundle.getString("name");
            Edit1.setText(String.valueOf(receiveValue1));
            final int receiveValue2 = receiveBundle.getInt("roll");
            Edit2.setText(Integer.toString(receiveValue2));
        }
    }



    public void click2(View view) {
        switch(view.getId()){

            case R.id.save:


                intent.putExtra("name",Edit1.getText().toString());
                intent.putExtra("roll",Integer.parseInt(Edit2.getText().toString()));

                setResult(RESULT_OK,intent);
                finish();

            case R.id.cancel:
                setResult(RESULT_CANCELED);
                finish();






        }




    }
}
