package com.example.shruti.calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;


import java.util.ArrayList;
import java.util.List;


public class Activity2 extends Activity {


    ListView listView;
    Intent intent;
    TestAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
       // intent = this.getIntent();
        listView = (ListView) findViewById(R.id.list_view);
        List<Calculator> list = (ArrayList<Calculator>)getIntent().getSerializableExtra("data");
        adapter = new TestAdapter(list , Activity2.this);
        listView.setAdapter(adapter);
    }
}
