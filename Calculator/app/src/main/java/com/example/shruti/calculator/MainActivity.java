package com.example.shruti.calculator;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends ActionBarActivity {


    EditText Edit1,Edit2;
    Double num1,num2,result;
    TextView output;
    ArrayList<Calculator> map=new ArrayList<Calculator>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Edit1=(EditText)findViewById(R.id.edit1);
        Edit2=(EditText)findViewById(R.id.edit2);
        output=(TextView)findViewById(R.id.result);


    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public  void clickCalc(View view)
    {
        switch (view.getId())
        {
            case R.id.sum_btn:
                num1= Double.parseDouble(Edit1.getText().toString());
                num2= Double.parseDouble(Edit2.getText().toString());
                result=num1+num2;
                output.setText(result+"");
                map.add(new Calculator(num1,"+",num2,result));
                break;

            case R.id.diff_btn:
                num1= Double.parseDouble(Edit1.getText().toString());
                num2= Double.parseDouble(Edit2.getText().toString());
                result=(num1-num2);
                output.setText(result+"");
                map.add(new Calculator(num1,"-",num2,result));

                break;
            case R.id.mul_btn:
                num1= Double.parseDouble(Edit1.getText().toString());
                num2= Double.parseDouble(Edit2.getText().toString());
                result=(num1*num2);
                output.setText(result+"");
                map.add(new Calculator(num1,"*",num2,result));

                break;

            case R.id.div_btn:

                num1= Double.parseDouble(Edit1.getText().toString());
                num2= Double.parseDouble(Edit2.getText().toString());
                if(num2!=0){
                result=(num1/num2);
                output.setText(result+"");
                    map.add(new Calculator(num1,"/",num2,result));
                }
                else
                {
                 divideAlert();

                }

                break;

            case R.id.summary:
                Intent intent = new Intent(this,Activity2.class);
                intent.putExtra("data",map);
                startActivity(intent);


                break;

             default:
                 break;

        }




    }
    protected void divideAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Division Alert");
        alertDialog.setMessage("\t\tCan't divide by Zero");
        alertDialog.show();

    }
}
