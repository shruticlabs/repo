package com.example.shruti.calculator;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestAdapter extends BaseAdapter {

    List<Calculator> map = new ArrayList<Calculator>();
    Context ctx;

    public TestAdapter(List<Calculator> map, Context ctx) {
        this.map = map;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return map.size();
    }

    @Override
    public Object getItem(int position) {
        return map.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.single_row,null);
        TextView i1 = (TextView)view.findViewById(R.id.final_view);
        i1.setText(map.get(position).toString());
        return view;

    }
}