package com.example.shruti.calculator;


import java.io.Serializable;

public class Calculator implements Serializable{

    double input1;
    double input2;
    double result;

    String operator;

    public Calculator(double input1, String operator, double input2, double result) {
        this.input1 = input1;
        this.input2 = input2;
        this.result = result;
        this.operator = operator;
    }

    @Override
    public String toString() {
        return input1 + " " + operator + " " + input2 + " = " + result;
    }
}
